import * as assert from 'assert';
import { html } from 'lit-html';
import { AnimationEditor } from '../../src/elements/animation-editor';
import { DataStore } from '../../src/interfaces/DataStore';
import { trender } from '../helpers';
import { Pallette } from '../../src/models/Pallette';

describe('animation-editor', () => {
  it('should set active color', () => {
    const el = new AnimationEditor({} as DataStore);
    // prettier-ignore
    trender(html`${el}`);

    el.setActiveColor(334);
    assert.strictEqual(el.activeColor, 334, 'should be the active color');
  });

  it('should get saved animation names', async () => {
    const store = {} as DataStore;
    const el = new AnimationEditor(store);
    // prettier-ignore
    trender(html`${el}`);

    store.getKeys = async s => {
      assert.strictEqual(s, 'animations', 'should be the animations store');
      return ['a', 'b'] as any;
    };

    const result = await el.getSavedAnimationNames();
    assert.deepStrictEqual(result, ['a', 'b'], 'should be the saved animation names');
  });

  it('should get saved pallette names', async () => {
    const store = {} as DataStore;
    const el = new AnimationEditor(store);
    // prettier-ignore
    trender(html`${el}`);

    store.getKeys = async s => {
      assert.strictEqual(s, 'pallettes', 'should be the pallettes store');
      return ['a', 'b'] as any;
    };

    const result = await el.getSavedPalletteNames();
    assert.deepStrictEqual(result, ['a', 'b'], 'should be the saved pallette names');
  });

  it('should get a Pallette by name', async () => {
    const store = {} as DataStore;
    const el = new AnimationEditor(store);
    // prettier-ignore
    trender(html`${el}`);

    store.hasKey = async (s, n) => {
      assert.strictEqual(s, 'pallettes', 'should be the store name');
      assert.strictEqual(n, 'test', 'should be the key name');
      return false;
    };

    store.get = async (s, n) => {
      assert.strictEqual(s, 'pallettes', 'should be the store name');
      assert.strictEqual(n, 'test', 'should be the key name');
      return new Pallette(n, store, [0]) as any;
    };

    const result = await el.getPallette('test');
    assert(result instanceof Pallette);
    assert.strictEqual(result.name, 'test', 'should have fetched the pallette');
  });
});
