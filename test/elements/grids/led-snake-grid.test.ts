import * as assert from 'assert';
import { trender, tqueryall } from '../../helpers';
import { LedSnakeGrid } from '../../../src/elements/grids/led-snake-grid';
import { AnimationEditor } from '../../../src/elements/animation-editor';
import { html } from 'lit-html';
import { LedCell } from '../../../src/elements/grids/led-cell';
import { NewFrame } from '../../../src/models/Frame';

describe('led-snake-grid', () => {
  it('should draw', () => {
    const grid = new LedSnakeGrid({} as AnimationEditor, 3, 2);
    trender(
      html`
        ${grid}
      `
    );
    grid.loadFrame(NewFrame([5, 4, 7, 3, 2, 8], 26));
    grid.draw();
    const rows = tqueryall('led-snake-grid-row');
    const cells = tqueryall('led-cell');

    assert.strictEqual(rows.length, 3, 'should be the number of rows');
    assert.strictEqual(cells.length, 6, 'should be the number of cells');
    assert(rows[0].classList.contains('even'));
    assert(rows[1].classList.contains('odd'));
    assert(rows[2].classList.contains('even'));
    assert.deepStrictEqual([...rows[0].querySelectorAll<LedCell>('led-cell')].map(x => x.color), [5, 4]);
    assert.deepStrictEqual([...rows[1].querySelectorAll<LedCell>('led-cell')].map(x => x.color), [7, 3]);
    assert.deepStrictEqual([...rows[2].querySelectorAll<LedCell>('led-cell')].map(x => x.color), [2, 8]);
  });
});
