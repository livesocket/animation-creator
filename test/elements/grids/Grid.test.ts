import * as assert from 'assert';
import { Grid } from '../../../src/elements/grids/Grid';
import { AnimationEditor } from '../../../src/elements/animation-editor';
import { Frame, NewFrame } from '../../../src/models/Frame';

class TestGrid extends Grid {
  public draw() {}

  public testSetFrame(frame: Frame) {
    this.frame = frame;
  }

  public testGetFrame(): Frame {
    return this.frame;
  }
}
window.customElements.define('test-grid', TestGrid);

describe('Grid', () => {
  it('should set generate cells on frame set', () => {
    const grid = new TestGrid({} as AnimationEditor);
    const frame = NewFrame([4, 5, 3, 2], 33);
    grid.testSetFrame(frame);
    assert.deepStrictEqual(grid.cells.map(x => x.color), [4, 5, 3, 2], 'should be the correct colors in cells');
  });

  it('should get active color from editor', () => {
    const editor = {} as AnimationEditor;
    const grid = new TestGrid(editor);

    editor.activeColor = 99;

    const result = grid.getActiveColor();
    assert.strictEqual(result, editor.activeColor, 'should be the editors active color');
  });

  it('should load a frame', () => {
    const grid = new TestGrid({} as AnimationEditor);
    const frame = NewFrame([1, 2, 3], 68);
    grid.loadFrame(frame);
    assert.strictEqual(frame, grid.testGetFrame(), 'should be the frame');
  });

  it('should save a cell to the frame', () => {
    const grid = new TestGrid({} as AnimationEditor);
    const frame = NewFrame([1, 2, 3], 68);
    grid.loadFrame(frame);
    grid.saveCell(1, 999);
    assert.deepStrictEqual(grid.testGetFrame().leds, [1, 999, 3], 'should be have saved the cell');
  });
});
