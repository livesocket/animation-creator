import * as assert from 'assert';
import { trender } from '../../helpers';
import { LedCell } from '../../../src/elements/grids/led-cell';
import { LedSnakeGrid } from '../../../src/elements/grids/led-snake-grid';
import { html } from 'lit-html';

describe('led-cell', () => {
  it('should set the background on color set', () => {
    const cell = new LedCell({} as LedSnakeGrid, 0);
    // prettier-ignore
    trender(html`${cell}`);

    assert.strictEqual(cell.style.backgroundColor, 'rgb(0, 0, 0)', 'should be the default black');
    cell.color = 10;
    assert.strictEqual(cell.style.backgroundColor, 'rgb(0, 0, 10)', 'should be the new color');
  });

  it('should set the color to the active', () => {
    const grid = {} as LedSnakeGrid;
    const cell = new LedCell(grid, 14);
    // prettier-ignore
    trender(html`${cell}`);

    grid.getActiveColor = () => 31;
    let saved = false;
    grid.saveCell = (idx, color) => {
      assert.strictEqual(idx, 14, 'should be the index');
      assert.strictEqual(color, 31, 'should be the active color');
      saved = true;
    };

    cell.setColor();
    assert.strictEqual(cell.color, 31, 'should be the active color');
    assert(saved);
  });

  it('should set active color on mousedown', done => {
    const grid = {} as LedSnakeGrid;
    const cell = new LedCell(grid, 14);
    // prettier-ignore
    trender(html`${cell}`);

    cell.setColor = () => done();

    cell.dispatchEvent(new MouseEvent('mousedown', { buttons: 0x1 }));
  });

  it('should set active color on mouseenter if button pressed', done => {
    const grid = {} as LedSnakeGrid;
    const cell = new LedCell(grid, 14);
    // prettier-ignore
    trender(html`${cell}`);

    cell.setColor = () => done();

    cell.dispatchEvent(new MouseEvent('mouseenter', { buttons: 0x1 }));
  });

  it('should not set active color on mousedown if not correct button', done => {
    const grid = {} as LedSnakeGrid;
    const cell = new LedCell(grid, 14);
    // prettier-ignore
    trender(html`${cell}`);

    cell.setColor = () => done('should not have set color');

    cell.dispatchEvent(new MouseEvent('mousedown', { buttons: 0x0 }));
    setTimeout(() => done(), 0);
  });

  it('should not set active color on mouseenter if not correct button', done => {
    const grid = {} as LedSnakeGrid;
    const cell = new LedCell(grid, 14);
    // prettier-ignore
    trender(html`${cell}`);

    cell.setColor = () => done('should not have set color');

    cell.dispatchEvent(new MouseEvent('mouseenter', { buttons: 0x0 }));
    setTimeout(() => done(), 0);
  });
});
