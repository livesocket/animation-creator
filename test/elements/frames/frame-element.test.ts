import * as assert from 'assert';
import { html } from 'lit-html';
import { FrameElement } from '../../../src/elements/frames/frame-element';
import { FrameSidebar } from '../../../src/elements/frames/frame-sidebar';
import { Frame } from '../../../src/models/Frame';
import { trender, tquery } from '../../helpers';

describe('frame-element', () => {
  it('should set active frame on click', done => {
    const sidebar = {} as FrameSidebar;
    const el = new FrameElement(sidebar, 2, {} as Frame);

    sidebar.setActive = async idx => {
      try {
        assert.strictEqual(idx, 2, 'should be the frame index');
        done();
      } catch (e) {
        done(e);
      }
    };

    // prettier-ignore
    trender(html`${el}`);
    el.dispatchEvent(new Event('click'));
  });

  it('should play animation from frame', async () => {
    const sidebar = {} as FrameSidebar;
    const el = new FrameElement(sidebar, 2, {} as Frame);

    let played = false;
    sidebar.playFromFrame = async idx => {
      assert.strictEqual(idx, 2, 'should be the frame index');
      played = true;
    };

    // prettier-ignore
    trender(html`${el}`);
    tquery('action:nth-of-type(1)').dispatchEvent(new Event('click'));
    assert(played);
  });

  it('should add frame', done => {
    const sidebar = {} as FrameSidebar;
    const el = new FrameElement(sidebar, 2, {} as Frame);

    sidebar.addFrame = async idx => {
      try {
        assert.strictEqual(idx, 2, 'should be the frame index');
        done();
      } catch (e) {
        done(e);
      }
    };

    // prettier-ignore
    trender(html`${el}`);
    tquery('action:nth-of-type(3)').dispatchEvent(new Event('click'));
  });

  it('should delete frame', done => {
    const sidebar = {} as FrameSidebar;
    const el = new FrameElement(sidebar, 2, {} as Frame);

    sidebar.deleteFrame = async idx => {
      try {
        assert.strictEqual(idx, 2, 'should be the frame index');
        done();
      } catch (e) {
        done(e);
      }
    };

    // prettier-ignore
    trender(html`${el}`);
    tquery('action:nth-of-type(4)').dispatchEvent(new Event('click'));
  });
});
