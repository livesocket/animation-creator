import * as assert from 'assert';
import { Frame } from '../../../src/models/Frame';
import { trender, tquery } from '../../helpers';
import { FrameDuration } from '../../../src/elements/frames/frame-duration';
import { html } from 'lit-html';

describe('frame-duration', () => {
  it('should load duration from frame', () => {
    const frame = { leds: [], duration: 22 } as Frame;
    const el = new FrameDuration();
    // prettier-ignore
    trender(html`${el}`);
    el.loadFrame(frame);
    const input = tquery<HTMLInputElement>('input');
    assert.strictEqual(input.value, '22', 'should be the value');
  });

  it('should load default duration if none provided', () => {
    const frame = { leds: [], duration: void 0 } as Frame;
    const el = new FrameDuration();
    // prettier-ignore
    trender(html`${el}`);
    el.loadFrame(frame);
    const input = tquery<HTMLInputElement>('input');
    assert.strictEqual(input.value, '100', 'should be the value');
  });

  it('should update the frame duration if the value changes', () => {
    const frame = { leds: [], duration: 22 } as Frame;
    const el = new FrameDuration();
    // prettier-ignore
    trender(html`${el}`);
    el.loadFrame(frame);
    const input = tquery<HTMLInputElement>('input');
    assert.strictEqual(input.value, '22', 'should be the value');
    input.value = '33';
    input.dispatchEvent(new Event('input'));
    assert.strictEqual(frame.duration, 33, 'should be the updated duration');
  });
});
