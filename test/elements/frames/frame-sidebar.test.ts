import * as assert from 'assert';
import { html } from 'lit-html';
import { FrameSidebar } from '../../../src/elements/frames/frame-sidebar';
import { AnimationEditor } from '../../../src/elements/animation-editor';
import { trender, tquery, tqueryall } from '../../helpers';
import { Frame, NewFrame } from '../../../src/models/Frame';
import { FrameElement } from '../../../src/elements/frames/frame-element';

describe('frame-sidebar', () => {
  it('should add frame on click of add frame button', async () => {
    const el = new FrameSidebar({} as AnimationEditor);
    // prettier-ignore
    trender(html`${el}`);
    const button = tquery('add-frame');
    let added = false;
    el.addFrame = async () => {
      added = true;
    };
    button.dispatchEvent(new Event('click'));
    assert(added);
  });

  it('should load frames', async () => {
    const el = new FrameSidebar({} as AnimationEditor);
    // prettier-ignore
    trender(html`${el}`);

    const frames = [{ leds: [], duration: 1 }, { leds: [], duration: 1 }, { leds: [], duration: 1 }] as Frame[];

    let activated = false;
    el.setActive = async idx => {
      assert.strictEqual(idx, 0, 'should set 0 as active');
      activated = true;
    };

    await el.load(frames);
    assert(activated);
    assert.strictEqual(tqueryall('frame-element').length, 3, 'should be 3 frame elements');
  });

  it('should set active frame', async () => {
    const editor = {} as AnimationEditor;
    const el = new FrameSidebar(editor);
    // prettier-ignore
    trender(html`${el}`);

    let saved = false;
    editor.saveAnimation = async () => {
      saved = true;
    };

    let loaded = false;
    editor.loadFrame = idx => {
      assert.strictEqual(idx, 5, 'should be index of 5');
      loaded = true;
    };

    el.frames = new Array(10).fill({ leds: [], duration: 1 }).map((x, i) => new FrameElement(el, i, x));

    await el.setActive(5);
    const fels = tqueryall('frame-element');
    assert(saved, 'should have saved');
    assert(loaded, 'should have loaded');
    assert(!fels[0].classList.contains('active'), '0 should not be active');
    assert(!fels[1].classList.contains('active'), '1 should not be active');
    assert(!fels[2].classList.contains('active'), '2 should not be active');
    assert(!fels[3].classList.contains('active'), '3 should not be active');
    assert(!fels[4].classList.contains('active'), '4 should not be active');
    assert(fels[5].classList.contains('active'), '5 should be active');
    assert(!fels[6].classList.contains('active'), '6 should not be active');
    assert(!fels[7].classList.contains('active'), '7 should not be active');
    assert(!fels[8].classList.contains('active'), '8 should not be active');
    assert(!fels[9].classList.contains('active'), '9 should not be active');
  });

  it('should play from frame', async () => {
    const editor = {} as AnimationEditor;
    const el = new FrameSidebar(editor);
    // prettier-ignore
    trender(html`${el}`);

    let played = false;
    editor.playAnimation = async idx => {
      assert.strictEqual(idx, 7, 'should be the index');
      played = true;
    };

    await el.playFromFrame(7);
    assert(played);
  });

  it('should add a frame', async () => {
    const editor = {} as AnimationEditor;
    const el = new FrameSidebar(editor);
    // prettier-ignore
    trender(html`${el}`);

    let added = false;
    editor.addFrame = after => {
      assert.strictEqual(after, 1, 'should be the correct index');
      added = true;
      return NewFrame([], 2);
    };

    let activated = false;
    el.setActive = async idx => {
      assert.strictEqual(idx, 2, 'should be the active index');
      activated = true;
    };

    el.frames = [NewFrame([], 0), NewFrame([], 1), NewFrame([], 3)].map((x, i) => new FrameElement(el, i, x));
    await el.addFrame(1);

    const frames = tqueryall<FrameElement>('frame-element');
    assert(added);
    assert(activated);
    assert.deepStrictEqual(frames.map(x => x.frame.duration), [0, 1, 2, 3], 'should be in the right order');
    assert.deepStrictEqual(frames.map(x => x.number), [0, 1, 2, 3], 'should be the right indexes');
  });

  it('should delete a frame', async () => {
    const editor = {} as AnimationEditor;
    const el = new FrameSidebar(editor);
    // prettier-ignore
    trender(html`${el}`);

    let deleted = false;
    editor.deleteFrame = idx => {
      assert.strictEqual(idx, 1, 'should be the delete index');
      deleted = true;
    };

    let activated = false;
    el.setActive = async idx => {
      assert.strictEqual(idx, 0, 'should be the new active frame');
      activated = true;
    };

    el.frames = [NewFrame([], 0), NewFrame([], 1), NewFrame([], 3)].map((x, i) => new FrameElement(el, i, x));
    await el.deleteFrame(1);
    const frames = tqueryall<FrameElement>('frame-element');
    assert(deleted);
    assert(activated);
    assert.deepStrictEqual(frames.map(x => x.frame.duration), [0, 3], 'should have deleted the correct item');
    assert.deepStrictEqual(frames.map(x => x.number), [0, 1], 'should have re assigned indexes');
  });
});
