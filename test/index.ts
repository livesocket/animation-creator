import * as assert from 'assert';
it('sanity check', () => {
  assert(true);
});

import './elements/frames/frame-duration.test';
import './elements/frames/frame-element.test';
import './elements/frames/frame-sidebar.test';

import './elements/grids/Grid.test';
import './elements/grids/led-cell.test';
import './elements/grids/led-snake-grid.test';

import './elements/animation-editor.test';

import './models/Animation.test';
import './models/Frame.test';
import './models/Pallette.test';
