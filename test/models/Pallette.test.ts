import * as assert from 'assert';
import { DataStore } from '../../src/interfaces/DataStore';
import { Pallette } from '../../src/models/Pallette';

describe('Pallette', () => {
  let store: DataStore;
  beforeEach(() => {
    store = {} as DataStore;
  });

  it('should get a new Pallette', async () => {
    store.hasKey = async () => false;

    store.get = async () => ({ name: 'test-pallette', colors: [] } as any);

    const result = await Pallette.get('test-pallette', store);
    assert(result instanceof Pallette);
    assert.strictEqual(result.name, 'test-pallette', 'should be the name');
    assert.deepStrictEqual(result.colors, [0], 'should have inital color of 0');
  });

  it('should get an existing Pallette', async () => {
    store.hasKey = async () => true;

    store.get = async () => ({ name: 'test-pallette', colors: [0, 255, 8271] } as any);

    const result = await Pallette.get('test-pallette', store);
    assert(result instanceof Pallette);
    assert.strictEqual(result.name, 'test-pallette', 'should be the name');
    assert.deepStrictEqual(result.colors, [0, 255, 8271], 'should have the colors');
  });

  it('should save a new Pallette', async () => {
    const pallette = new Pallette('t', store, [1, 2]);

    store.hasKey = async () => false;

    let saved = false;
    store.create = async (s, item) => {
      assert.strictEqual(s, 'pallettes', 'should be the store name');
      assert.strictEqual(item, pallette, 'should be the pallette to save');
      saved = true;
    };

    await pallette.save();
    assert(saved);
  });

  it('should save a new Pallette', async () => {
    const pallette = new Pallette('t', store, [1, 2]);

    store.hasKey = async () => true;

    let saved = false;
    store.update = async (s, item) => {
      assert.strictEqual(s, 'pallettes', 'should be the store name');
      assert.strictEqual(item, pallette, 'should be the pallette to save');
      saved = true;
    };

    await pallette.save();
    assert(saved);
  });
});
