import * as assert from 'assert';
import { Animation } from '../../src/models/Animation';
import { DataStore } from '../../src/interfaces/DataStore';
import { NewFrame } from '../../src/models/Frame';

describe('Animation', () => {
  let store: DataStore;

  beforeEach(() => {
    store = {} as DataStore;
  });

  it('should get a new animation', async () => {
    const name = 'test-animation';
    store.get = async (s, n) => {
      assert.strictEqual(s, 'animations', 'animations should be the store name');
      assert.strictEqual(n, name, `${name} should be the animation name`);
      return Promise.resolve(void 0);
    };
    const result = await Animation.get('test-animation', store);
    assert(result instanceof Animation, 'should be an instance of Animation');
    assert.deepStrictEqual(result.frames, [], 'frames should be empty');
  });

  it('should load an existing animation', async () => {
    const expected = { name: 'test-animation', frames: [{ leds: [1], duration: 44 }] } as Animation;
    store.get = async (s, n) => {
      assert.strictEqual(s, 'animations', 'animations should be the store name');
      assert.strictEqual(n, expected.name, `${expected.name} should be the animation name`);
      return Promise.resolve(expected) as any;
    };

    const result = await Animation.get('test-animation', store);
    assert(result instanceof Animation, 'should be an instance of Animation');
    assert.strictEqual(result.name, expected.name, 'should be the expected animation');
    assert.deepStrictEqual(result.frames, expected.frames, 'should be the expected frames');
  });

  it('should add a frame', () => {
    const animation = new Animation('t', store);
    animation.frames = [NewFrame([11], 75)];
    animation.addFrame(0);
    assert.deepStrictEqual(animation.frames, [{ leds: [11], duration: 75 }, { leds: [11], duration: 75 }]);
  });

  it('should delete a frame', () => {
    const frames = [NewFrame([11], 75), NewFrame([22], 85), NewFrame([33], 95)];
    const animation = new Animation('t', store);
    animation.frames = frames.slice(0);
    animation.deleteFrame(1);
    assert.deepStrictEqual(animation.frames, [frames[0], frames[2]], 'should have removed frame frames');
  });

  it('should get a frame', () => {
    const frames = [NewFrame([11], 75), NewFrame([22], 85), NewFrame([33], 95)];
    const animation = new Animation('t', store);
    animation.frames = frames.slice(0);
    const result = animation.getFrame(1);
    assert.deepStrictEqual(result, frames[1], 'should be the requested frame');
  });

  it('should save a new animation', async () => {
    const frames = [NewFrame([11], 75), NewFrame([22], 85), NewFrame([33], 95)];
    const animation = new Animation('t', store);
    animation.frames = frames.slice(0);

    store.hasKey = async s => {
      assert.strictEqual(s, 'animations', 'should be the key');
      return false;
    };

    let created = false;
    store.create = async (s, item) => {
      assert.strictEqual(s, 'animations', 'should be the key');
      assert.strictEqual(item, animation, 'should be the animation');
      created = true;
    };

    await animation.save();
    assert(created, 'should have been created');
  });

  it('should save an existing animation', async () => {
    const frames = [NewFrame([11], 75), NewFrame([22], 85), NewFrame([33], 95)];
    const animation = new Animation('t', store);
    animation.frames = frames.slice(0);

    store.hasKey = async (s, n) => {
      assert.strictEqual(s, 'animations', 'should be the key');
      assert.strictEqual(n, 't', 'should be the name');
      return true;
    };

    let updated = false;
    store.update = async (s, item) => {
      assert.strictEqual(s, 'animations', 'should be the key');
      assert.strictEqual(item, animation, 'should be the animation');
      updated = true;
    };

    await animation.save();
    assert(updated, 'should have been updated');
  });
});
