import * as assert from 'assert';
import { NewFrame, NewEmptyFrame, NewFrameFrom } from '../../src/models/Frame';

describe('Frame', () => {
  it('should create a new frame', () => {
    const result = NewFrame([25, 36], 22);
    assert.deepStrictEqual(result, { leds: [25, 36], duration: 22 }, 'should be the new frame');
  });

  it('should create a new empty frame', () => {
    const result = NewEmptyFrame(5);
    assert.deepStrictEqual(result.leds, [0, 0, 0, 0, 0], 'led length should be 5');
  });

  it('should create a new frame from existing frame', () => {
    const existing = NewFrame([2, 3, 4], 88);
    const result = NewFrameFrom(existing);
    assert.deepStrictEqual(result, existing, 'frame data should be the same');
    assert.notStrictEqual(result, existing, 'should not be the same object');
  });
});
