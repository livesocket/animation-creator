import { DataStore } from '../../src/interfaces/DataStore';

export class TestDataStore implements DataStore {
  private store = new Map<string, Map<string, any>>();

  public async open(): Promise<DataStore> {
    return this;
  }

  public async get<T>(store: string, key: string): Promise<T> {
    return this.for(store).get(key);
  }

  public async getKeys<T extends []>(store: string): Promise<T> {
    return [...this.for(store).keys()] as T;
  }

  public async hasKey(store: string, key: string): Promise<boolean> {
    return this.for(store).has(key);
  }

  public async create(store: string, item: any): Promise<void> {
    this.for(store).set(item.name, item);
  }

  public async update(store: string, item: any): Promise<void> {
    this.for(store).set(item.name, item);
  }

  private for(store: string) {
    if (!this.store.has(store)) {
      this.store.set(store, new Map());
    }
    return this.store.get(store);
  }
}
