import { TemplateResult, render } from 'lit-html';
declare const TEST_CONTAINER: HTMLElement;

export function trender(template: TemplateResult) {
  return render(template, TEST_CONTAINER);
}

export function tquery<T extends HTMLElement>(selector: string): T {
  return TEST_CONTAINER.querySelector<T>(selector);
}

export function tqueryall<T extends HTMLElement>(selector: string): T[] {
  return [...TEST_CONTAINER.querySelectorAll<T>(selector)];
}
