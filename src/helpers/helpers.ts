export function numberToColor(color: number): string {
  let str = color.toString(16);
  while (str.length < 6) {
    str = '0' + str;
  }
  return '#' + str;
}

export function colorToNumber(color: string): number {
  return Number(color.replace('#', '0x'));
}
