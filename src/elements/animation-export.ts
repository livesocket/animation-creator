import { render, html } from 'lit-html';
import { AnimationEditor } from './animation-editor';

export class AnimationExport extends HTMLElement {
  public text: string = 'Export';

  constructor(private editor: AnimationEditor) {
    super();
  }

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    // prettier-ignore
    render(html`<button type="button" @click=${() => this.export()}>${this.text}</button>`, this);
  }

  public export() {
    navigator.clipboard.writeText(JSON.stringify(this.editor.animation));
    this.text = 'Copied to Clipboard!';
    this.classList.add('copied');
    this.draw();
    setTimeout(() => {
      this.text = 'Export';
      this.classList.remove('copied');
      this.draw();
    }, 3000);
  }
}
window.customElements.define('animation-export', AnimationExport);
