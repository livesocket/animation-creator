import { render, html } from 'lit-html';
import { AnimationEditor } from '../animation-editor';
import { Frame } from '../../models/Frame';
import { FrameElement } from './frame-element';

export class FrameSidebar extends HTMLElement {
  public frames: FrameElement[] = [];

  constructor(private editor: AnimationEditor) {
    super();
  }

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    // prettier-ignore
    render(html`${this.frames}<add-frame @click=${() => this.addFrame()}>+ Add Frame</add-frame>`, this);
  }

  public async load(frames: Frame[]) {
    this.frames = frames.map((x, i) => new FrameElement(this, i, x));
    await this.setActive(0);
    this.draw();
  }

  public async setActive(idx: number) {
    await this.editor.saveAnimation();
    this.editor.loadFrame(idx);
    this.frames.forEach((x, i) => (i === idx ? x.classList.add('active') : x.classList.remove('active')));
    this.draw();
  }

  public async playFromFrame(idx: number): Promise<void> {
    return this.editor.playAnimation(idx);
  }

  public async addFrame(after: number = this.frames.length - 1) {
    const frame = this.editor.addFrame(after);
    this.frames.splice(after + 1, 0, new FrameElement(this, after + 1, frame));
    this.frames.forEach((x, i) => (x.number = i));
    await this.setActive(after + 1);
    this.draw();
  }

  public async deleteFrame(idx: number) {
    this.editor.deleteFrame(idx);
    this.frames.splice(idx, 1);
    this.frames.forEach((x, i) => (x.number = i));
    await this.setActive(idx - 1);
    this.draw();
  }
}
window.customElements.define('frame-sidebar', FrameSidebar);
