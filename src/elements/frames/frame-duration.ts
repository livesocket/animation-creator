import { render, html } from 'lit-html';
import { Frame } from '../../models/Frame';

export class FrameDuration extends HTMLElement {
  private frame: Frame;

  private get input(): HTMLInputElement {
    return this.querySelector('input');
  }

  public connectedCallback() {
    // prettier-ignore
    render(html`Frame Duration (ms) <input type="number" placeholder="ms" @input=${(event: any) => !!this.frame ? this.frame.duration = +event.target.value : void 0} />`, this);
  }

  public loadFrame(frame: Frame) {
    this.frame = frame;
    if (typeof this.frame.duration === 'undefined') {
      this.frame.duration = 100;
    }
    if (!!this.input) {
      this.input.value = '' + this.frame.duration;
    } else {
      const interval = setInterval(() => {
        if (!!this.input) {
          this.input.value = '' + this.frame.duration;
          clearInterval(interval);
        }
      }, 5);
    }
  }
}
window.customElements.define('frame-duration', FrameDuration);
