const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { Frame } from '../../models/Frame';
import { FrameSidebar } from './frame-sidebar';

const template = (e: FrameElement) => html`
  <name contenteditable="false" @keydown=${(v: KeyboardEvent) => e.saveName(v)} @input=${(v: Event) => e.setName(v)}>
    ${e.frame.name ? e.frame.name : `Frame ${e.number}`}
  </name>
  <actions>
    <action @click=${(v: Event) => e.play(v)}>
      ${unsafeHTML(octicons['play'].toSVG())}
    </action>
    <action @click=${(v: Event) => e.edit(v)}>
      ${unsafeHTML(octicons['pencil'].toSVG())}
    </action>
    <action @click=${(v: Event) => e.add(v)}>
      ${unsafeHTML(octicons['plus'].toSVG())}
    </action>
    <action @click=${(v: Event) => e.delete(v)}>
      ${unsafeHTML(octicons['trashcan'].toSVG())}
    </action>
  </actions>
`;

export class FrameElement extends HTMLElement {
  constructor(private sidebar: FrameSidebar, public number: number, public frame: Frame) {
    super();
  }

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(template(this), this);
    this.addEventListener('click', () => this.sidebar.setActive(this.number));
  }

  public setName(event: Event) {
    event.stopPropagation();
    this.frame.name = (event.target as HTMLElement).innerText;
  }

  public saveName(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.querySelector<HTMLInputElement>('name').contentEditable = 'false';
    }
  }

  public async play(event: Event) {
    event.stopPropagation();
    return this.sidebar.playFromFrame(this.number);
  }

  public edit(event: Event) {
    event.stopPropagation();
    const name = this.querySelector('name') as HTMLInputElement;
    name.contentEditable = 'true';
    name.focus();
    document.execCommand('selectAll');
  }

  public async add(event: Event) {
    event.stopPropagation();
    return this.sidebar.addFrame(this.number);
  }

  public delete(event: Event) {
    event.stopPropagation();
    this.sidebar.deleteFrame(this.number);
  }
}
window.customElements.define('frame-element', FrameElement);
