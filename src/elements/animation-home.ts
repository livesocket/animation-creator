import { render, html } from 'lit-html';

const template = (e: AnimationHome) => html`
  <animation-editor></animation-editor>
`;

class AnimationHome extends HTMLElement {
  public connectedCallback() {
    render(template(this), this);
  }
}

window.customElements.define('animation-home', AnimationHome);
