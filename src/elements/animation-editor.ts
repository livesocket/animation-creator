const octicons = require('octicons');
import { render, html } from 'lit-html';
import { LedSnakeGrid } from './grids/led-snake-grid';
import { Frame } from '../models/Frame';
import { Animation } from '../models/Animation';
import { AnimationSelector } from './animation-selector';
import { PalletteSidebar } from './pallette/pallette-sidebar';
import { FrameSidebar } from './frames/frame-sidebar';
import { FrameDuration } from './frames/frame-duration';
import { AnimationToolbar } from './animation-toolbar';
import { DataStore } from '../interfaces/DataStore';
import { Pallette } from '../models/Pallette';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';

const template = (e: AnimationEditor) => html`
  ${!e.animation
    ? html`
        ${e.selector}
      `
    : html`
        ${e.toolbar}
        <main-area>
          ${e.frameSidebar}
          <editor-area>
            <editor-box>
              ${e.grid}
              <button-area>
                ${e.duration}
                <play-button @click=${() => e.playAnimation()}>
                  Play Animation ${unsafeHTML(octicons['play'].toSVG())}
                </play-button>
              </button-area>
            </editor-box>
          </editor-area>
          <playback-area>
            ${e.playbackGrid}
            <button type="button" @click=${() => e.stopAnimation()}>Stop Animation</button>
          </playback-area>
          ${e.pallette}
        </main-area>
      `}
`;

export class AnimationEditor extends HTMLElement {
  public activeColor: number;
  public toolbar: AnimationToolbar;
  public frameSidebar = new FrameSidebar(this);
  public grid = new LedSnakeGrid(this, 5, 10);
  public selector = new AnimationSelector(this);
  public pallette = new PalletteSidebar(this);
  public playbackGrid = new LedSnakeGrid(this, 5, 10);
  public duration = new FrameDuration();
  public animation: Animation;
  public currentFrame = 0;

  public get playing(): boolean {
    return this.classList.contains('playing');
  }

  public set playing(value: boolean) {
    this.classList[value ? 'add' : 'remove']('playing');
  }

  constructor(private store: DataStore) {
    super();
  }

  public async connectedCallback() {
    await this.store.open();
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public setActiveColor(color: number) {
    this.activeColor = color;
  }

  public async getSavedAnimationNames(): Promise<string[]> {
    return this.store.getKeys('animations');
  }

  public async getSavedPalletteNames(): Promise<string[]> {
    return this.store.getKeys('pallettes');
  }

  public async getPallette(name: string): Promise<Pallette> {
    return Pallette.get(name, this.store);
  }

  public async loadAnimation(name: string): Promise<void> {
    this.animation = await Animation.get(name, this.store);
    this.toolbar = new AnimationToolbar(this, name);
    this.frameSidebar.load(this.animation.frames);
    this.loadFrame(0);
  }

  public async saveAnimation() {
    try {
      await this.animation.save();
      this.draw();
    } catch (e) {
      alert('Error saving animation');
      throw e;
    }
  }

  public async playAnimation(idx: number = 0) {
    this.playing = true;
    for (const frame of this.animation.frames.slice(idx)) {
      if (!this.playing) {
        return;
      }
      await this.playFrame(frame);
    }
    this.stopAnimation();
  }

  public async stopAnimation() {
    this.playing = false;
  }

  public loadFrame(idx: number) {
    this.currentFrame = idx;
    const frame = this.animation.getFrame(idx);
    this.grid.loadFrame(frame);
    this.duration.loadFrame(frame);
    this.draw();
  }

  public async playFrame(frame: Frame) {
    return new Promise(resolve => {
      this.playbackGrid.loadFrame(frame);
      setTimeout(resolve, frame.duration);
    });
  }

  public addFrame(after: number): Frame {
    const frame = this.animation.addFrame(after);
    this.loadFrame(after + 1);
    return frame;
  }

  public deleteFrame(idx: number) {
    this.animation.deleteFrame(idx);
  }

  public async exit() {
    await this.saveAnimation();
    this.selector = new AnimationSelector(this);
    this.animation = void 0;
    this.draw();
  }
}
window.customElements.define('animation-editor', AnimationEditor);
