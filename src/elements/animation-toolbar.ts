const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { AnimationEditor } from './animation-editor';
import { AnimationExport } from './animation-export';

export class AnimationToolbar extends HTMLElement {
  constructor(private editor: AnimationEditor, private name: string) {
    super();
  }

  public connectedCallback() {
    render(
      html`
        <back-button @click=${() => this.editor.exit()}>${unsafeHTML(octicons['arrow-left'].toSVG())}</back-button>
        <p>${this.name}</p>
        ${new AnimationExport(this.editor)}
      `,
      this
    );
  }
}
window.customElements.define('animation-toolbar', AnimationToolbar);
