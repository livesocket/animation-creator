import { AnimationEditor } from './animation-editor';
import { render, html } from 'lit-html';
import { Animation } from '../models/Animation';

const template = (e: AnimationSelector) => html`
  ${!e.editor.animation
    ? html`
        <h1>Choose Animation</h1>
        <ul>
          ${e.animations.map(
            x =>
              html`
                <li @click=${() => e.load(x)}>${x}</li>
              `
          )}
        </ul>
        <form-area>
          <input type="text" placeholder="New Animation Name" @input=${(event: any) => (e.name = event.target.value)} />
          <button type="button" @click=${() => e.load(e.name)}>New Animation</button>
        </form-area>
      `
    : ''}
`;

export class AnimationSelector extends HTMLElement {
  public animations: string[];
  public name: string;

  constructor(public editor: AnimationEditor) {
    super();
  }

  public async connectedCallback() {
    this.animations = await this.getSavedAnimationNames();
    this.draw();
  }

  public draw() {
    render(template(this), this);
  }

  public async load(name: string) {
    try {
      await this.editor.loadAnimation(name);
    } catch (e) {
      console.log(e);
      alert('Unable to load animation');
    }
    this.draw();
  }

  public getSavedAnimationNames(): Promise<string[]> {
    return this.editor.getSavedAnimationNames();
  }
}
window.customElements.define('animation-selector', AnimationSelector);
