const octicons = require('octicons');
import { render, html } from 'lit-html';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { PalletteSidebar } from './pallette-sidebar';
import { numberToColor } from '../../helpers/helpers';

export class PalletteColor extends HTMLElement {
  constructor(protected sidebar: PalletteSidebar, public color: number) {
    super();
  }

  public connectedCallback() {
    render(
      html`
        <color-swatch
          style="background-color:${numberToColor(this.color)}"
          @click=${() => this.sidebar.setActive(this.color)}
        ></color-swatch>
        <p>${this.color === 0 ? 'OFF' : numberToColor(this.color)}</p>
        <color-actions>
          <color-action @click=${(e: Event) => this.delete(e)}>
            ${unsafeHTML(octicons['trashcan'].toSVG())}
          </color-action>
        </color-actions>
      `,
      this
    );
  }

  public delete(event: Event) {
    event.stopPropagation();
    this.sidebar.deleteColor(this);
    this.remove();
  }
}
window.customElements.define('pallette-color', PalletteColor);
