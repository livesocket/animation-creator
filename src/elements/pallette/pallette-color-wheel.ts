const iro = require('@jaames/iro').default;
import { render, html } from 'lit-html';
import { PalletteSidebar } from './pallette-sidebar';
import { colorToNumber } from '../../helpers/helpers';

export class PalletteColorWheel extends HTMLElement {
  public wheel: any;
  public color: string;

  constructor(private sidebar: PalletteSidebar) {
    super();
  }

  public connectedCallback() {
    // prettier-ignore
    render(html`
			<div id="color-wheel"></div>
			<add-color @click=${() => this.sidebar.addColor(colorToNumber(this.color))}>+ Add selected color</add-color>
    `, this);
    if (!this.wheel) {
      this.wheel = iro.ColorPicker('#color-wheel', {
        width: 259
      });
      this.wheel.on('color:change', (color: any) => this.sidebar.setActive((this.color = color.hexString)));
    }
  }
}
window.customElements.define('pallette-color-wheel', PalletteColorWheel);
