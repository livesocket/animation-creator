import { render, html } from 'lit-html';
import { AnimationEditor } from '../animation-editor';
import { PalletteColorWheel } from './pallette-color-wheel';
import { PalletteColor } from './pallette-color';
import { PalletteSelector } from './pallette-selector';
import { Pallette } from '../../models/Pallette';

export class PalletteSidebar extends HTMLElement {
  public colorWheel = new PalletteColorWheel(this);
  public colors: PalletteColor[] = [];
  public activePallette: Pallette;

  constructor(private editor: AnimationEditor) {
    super();
  }

  public connectedCallback() {
    this.draw();
  }

  public draw() {
    render(
      html`
        ${!!this.activePallette
          ? html`
              ${this.colorWheel}<pallette-colors>${this.colors}</pallette-colors>
            `
          : html`
              ${new PalletteSelector(this)}
            `}
      `,
      this
    );
  }

  public setActivePallette(pallette: Pallette) {
    this.activePallette = pallette;
    this.colors = this.activePallette.colors.map(x => new PalletteColor(this, x));
    this.draw();
  }

  public setActive(color: number) {
    this.editor.setActiveColor(color);
  }

  public async addColor(color: number) {
    if (typeof color == 'undefined') {
      return;
    }
    if (this.colors.some(x => x.color === color)) {
      return;
    }
    this.activePallette.colors.push(color);
    this.colors.push(new PalletteColor(this, color));
    this.draw();
    this.setActive(color);
    return this.activePallette.save();
  }

  public async deleteColor(color: PalletteColor) {
    this.colors = this.colors.filter(x => x !== color);
    this.draw();
    return this.activePallette.save();
  }

  public async getSavedPalletteNames(): Promise<string[]> {
    return this.editor.getSavedPalletteNames();
  }

  public async getPallette(name: string): Promise<Pallette> {
    return this.editor.getPallette(name);
  }
}
window.customElements.define('pallette-sidebar', PalletteSidebar);
