import { render, html } from 'lit-html';
import { Pallette } from '../../models/Pallette';
import { PalletteSidebar } from './pallette-sidebar';

const template = (e: PalletteSelector) => html`
  <p>Choose Pallette</p>
  <ul>
    ${e.names.map(
      x =>
        html`
          <li @click=${() => e.setActive(x)}>${x}</li>
        `
    )}
  </ul>
  <form-area>
    <input type="text" placeholder="New Pallette Name" @input=${(event: any) => (e.name = event.target.value)} />
    <add-pallette @click=${() => e.setActive(e.name)}>+ Add Pallette</add-pallette>
  </form-area>
`;

export class PalletteSelector extends HTMLElement {
  public names: string[] = [];
  public name: string;
  public active: Pallette;

  constructor(private sidebar: PalletteSidebar) {
    super();
  }

  public async connectedCallback() {
    this.names = await this.sidebar.getSavedPalletteNames();
    this.draw();
  }

  public draw() {
    // prettier-ignore
    render(template(this), this);
  }

  public async setActive(name: string) {
    this.active = await this.sidebar.getPallette(name);
    this.sidebar.setActivePallette(this.active);
  }
}
window.customElements.define('pallette-selector', PalletteSelector);
