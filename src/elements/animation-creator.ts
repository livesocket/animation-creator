import { render, html } from 'lit-html';
import { AnimationEditor } from './animation-editor';
import { AnimationDataStore } from '../models/db';

export class AnimationCreator extends HTMLElement {
  public connectedCallback() {
    // prettier-ignore
    render(html`${new AnimationEditor(new AnimationDataStore())}`, this);
  }
}
window.customElements.define('animation-creator', AnimationCreator);
