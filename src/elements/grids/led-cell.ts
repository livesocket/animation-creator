import { numberToColor } from '../../helpers/helpers';
import { Grid } from './Grid';

export class LedCell extends HTMLElement {
  public get color(): number {
    return this._color;
  }

  public set color(value: number) {
    this._color = value;
    this.style.backgroundColor = numberToColor(value);
  }

  private _color: number;
  constructor(private grid: Grid, public idx: number, color: number = 0) {
    super();
    this.color = color;
  }

  public connectedCallback() {
    function handle(event: MouseEvent) {
      if ((event.buttons & 0x1) === 0x1) {
        this.setColor();
      }
    }
    this.addEventListener('mousedown', handle);
    this.addEventListener('mouseenter', handle);
  }

  public setColor() {
    const color = this.grid.getActiveColor();
    if (typeof color === 'undefined') {
      return alert('Please select a color from the pallette on the right.');
    }
    this.color = color;
    this.grid.saveCell(this.idx, this.color);
  }
}
window.customElements.define('led-cell', LedCell);
