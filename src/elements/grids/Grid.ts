import { LedCell } from './led-cell';
import { AnimationEditor } from '../animation-editor';
import { Frame } from '../../models/Frame';

export abstract class Grid extends HTMLElement {
  public cells: LedCell[] = [];
  protected get frame(): Frame {
    return this._frame;
  }

  protected set frame(value: Frame) {
    this.cells = value.leds.map((x, i) => new LedCell(this, i, x));
    this._frame = value;
  }

  private _frame: Frame;

  constructor(protected editor: AnimationEditor) {
    super();
  }

  public abstract draw(): any;

  public getActiveColor(): number {
    return this.editor.activeColor;
  }

  public loadFrame(frame: Frame) {
    this.frame = frame;
    this.draw();
  }

  public saveCell(idx: number, color: number) {
    this.frame.leds[idx] = color;
  }
}
