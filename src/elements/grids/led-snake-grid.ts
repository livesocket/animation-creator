import { html, render } from 'lit-html';
import { AnimationEditor } from '../animation-editor';
import { Frame } from '../../models/Frame';
import { Animation } from '../../models/Animation';
import { Grid } from './Grid';

export class LedSnakeGrid extends Grid {
  public playing = false;

  constructor(editor: AnimationEditor, private rows: number, private columns: number) {
    super(editor);
  }

  public draw() {
    const parts = [];
    for (let row = 0; row < this.rows; row++) {
      parts.push(html`
        <led-snake-grid-row class=${row % 2 === 0 ? 'even' : 'odd'}>
          ${this.cells.slice(row * this.columns, row * this.columns + this.columns)}
        </led-snake-grid-row>
      `);
    }
    // prettier-ignore
    render(html`${parts}`, this);
  }
}
window.customElements.define('led-snake-grid', LedSnakeGrid);
