export interface DataStore {
  open(): Promise<DataStore>;
  get<T>(store: string, key: string): Promise<T>;
  getKeys<T extends []>(store: string): Promise<T>;
  hasKey(store: string, key: string): Promise<boolean>;
  create(store: string, item: any): Promise<void>;
  update(store: string, item: any): Promise<void>;
}
