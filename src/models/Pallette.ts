import { DataStore } from '../interfaces/DataStore';

export class Pallette {
  public static async get(name: string, store: DataStore): Promise<Pallette> {
    if (await store.hasKey('pallettes', name)) {
      const json = await store.get<Pallette>('pallettes', name);
      return new Pallette(json.name, store, json.colors);
    } else {
      return new Pallette(name, store, [0]);
    }
  }

  constructor(public name: string, private store: DataStore, public colors: number[] = []) {}

  public async save(): Promise<void> {
    if (await this.store.hasKey('pallettes', this.name)) {
      return this.store.update('pallettes', this);
    } else {
      return this.store.create('pallettes', this);
    }
  }
}
