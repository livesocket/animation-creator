import { DataStore } from '../interfaces/DataStore';

const updates = {
  '0': (db: IDBDatabase) => {
    db.createObjectStore('animations', { keyPath: 'name' });
  },
  '1': (db: IDBDatabase) => {
    db.createObjectStore('pallettes', { keyPath: 'name' });
  }
};

export class AnimationDataStore implements DataStore {
  private db: IDBDatabase;
  public async open(): Promise<this> {
    this.db = await new Promise((resolve, reject) => {
      const request = window.indexedDB.open('animation-creator', 2);
      request.onerror = () => reject('Error loading database');
      request.onsuccess = () => resolve(request.result);
      request.onupgradeneeded = event => resolve(this.upgrade(request, event));
    });
    return this;
  }

  private async upgrade(request: IDBOpenDBRequest, event: IDBVersionChangeEvent): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const db = request.result;
      db.onerror = () => reject('Error loading database');
      for (let i = event.oldVersion || 0; i < event.newVersion; i++) {
        updates[i](db);
      }
      resolve(db);
    });
  }

  private store(name: string, write: boolean = false): IDBObjectStore {
    return this.db.transaction(name, write ? 'readwrite' : 'readonly').objectStore(name);
  }

  public async get<T>(store: string, key: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      const request = this.store(store).get(key);
      request.onsuccess = () => resolve(request.result);
      request.onerror = e => reject(e);
    });
  }

  public async getKeys<T>(store: string): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      const request = this.store(store).getAllKeys();
      request.onsuccess = () => resolve((request.result as unknown) as T);
      request.onerror = () => reject();
    });
  }

  public async hasKey(store: string, key: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const request = this.store(store).getKey(key);
      request.onsuccess = () => resolve(!!request.result);
      request.onerror = e => reject(e);
    });
  }

  public async create(store: string, item: any): Promise<void> {
    return new Promise((resolve, reject) => {
      const request = this.store(store, true).add(item);
      request.onsuccess = () => resolve();
      request.onerror = e => reject(e);
    });
  }

  public async update(store: string, item: any): Promise<void> {
    return new Promise((resolve, reject) => {
      const request = this.store(store, true).put(item);
      request.onsuccess = () => resolve();
      request.onerror = () => reject();
    });
  }
}
