export interface Frame {
  name: string;
  leds: number[];
  duration: number; // ms
}

export function NewFrame(leds: number[], duration: number): Frame {
  return { name: void 0, leds, duration };
}

export function NewEmptyFrame(size: number): Frame {
  return {
    name: void 0,
    leds: new Array(size).fill(0),
    duration: void 0
  };
}

export function NewFrameFrom(frame: Frame): Frame {
  return { name: void 0, leds: frame.leds.slice(0), duration: frame.duration };
}
