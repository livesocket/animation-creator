import { Frame, NewFrameFrom } from './Frame';
import { DataStore } from '../interfaces/DataStore';

export class Animation {
  public static async get(name: string, store: DataStore): Promise<Animation> {
    const animation = await store.get<any>('animations', name);
    if (!!animation) {
      const result = new Animation(animation.name, store);
      result.frames = animation.frames;
      return result;
    } else {
      return new Animation(name, store);
    }
  }

  // public static async exists(name: string): Promise<boolean> {
  //   return DB.hasKey('animations', name);
  // }

  // public static async names(): Promise<string[]> {
  //   return DB.getKeys<string[]>('animations');
  // }

  public frames: Frame[] = [];
  public loop: number;

  constructor(public name: string, private store: DataStore) {}

  public async save(): Promise<void> {
    const animation = { name: this.name, frames: this.frames };
    if (await this.store.hasKey('animations', this.name)) {
      return this.store.update('animations', animation);
    }
    return this.store.create('animations', animation);
  }

  public getFrame(idx: number) {
    return this.frames[idx];
  }

  public addFrame(after: number) {
    const frame = NewFrameFrom(this.frames[after]);
    this.frames.splice(after, 0, frame);
    return frame;
  }

  public deleteFrame(idx: number) {
    this.frames.splice(idx, 1);
  }
}
