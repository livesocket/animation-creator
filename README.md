# animation-creator

https://livesocket.gitlab.io/animation-creator

This is the Animation Creator for animations that run using the [led-lib](https://gitlab.com/livesocket/led-lib).
This project is fully open sourced, in active development, and accepting merge requests. Instructions for building
and testing are included below.

## Setup

To setup your environment for development of this project make sure you have node and npm installed. Run the following to install dependencies.

```bash
$ npm install
```

## Building

To build the code run the following command

```bash
$ npm run build
```

## Running

To run the application in a browser first build the code, then use a web server to serve the index.html file.
If your not sure how to do that, you can use `http-server`, but this method is not required

```bash
# install http-server globally
$ npm install -g http-server

# To start the server run in same folder as the index.html file of this project
$ http-server -c-1
```

## Testing

To run unit tests for this project use the standard npm test command

```bash
$ npm test
```
