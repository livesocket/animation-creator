const path = require('path');

module.exports = {
  mode: 'production',

  entry: {
    bundle: './dist/src/index.js',
    style: './src/index.css',
    tests: './dist/test/index.js'
  },

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: '[name].js'
  },
  externals: ['autobahn'],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      }
    ]
  }
};
